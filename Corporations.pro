TEMPLATE = app

QT += qml quick network
CONFIG += c++11

SOURCES += main.cpp \
    map_maker.cpp \
    game_space.cpp \
    client.cpp \
    server.cpp \
    logic.cpp

RESOURCES += \
    qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    map_maker.h \
    game_space.h \
    client.h \
    server.h \
    enum_types.h \
    logic.h
